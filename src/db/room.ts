import { Schema, model, Document } from 'mongoose';
import { BaseTime, preSaveAddBaseTime } from './base';

export interface Room {
    name: string;
    description: string;
}

export interface RoomModel extends Room, BaseTime, Document { }

const modelSchema = new Schema({
    name: { type: String, required: false },
    description: String
});

modelSchema.pre('save', preSaveAddBaseTime);

////// Create Model /////

export const RoomModel = model<RoomModel>('Room', modelSchema);

////// Functions ////////

export function getRooms(limit = 100) {
    return RoomModel.find().limit(limit);
}

export function addRoom(input: Room) {
    const rec = new RoomModel(input);
    rec.save();
    return rec;
}

export function removeRoom(root, { id }) {
    return RoomModel.findByIdAndRemove(id);
}
