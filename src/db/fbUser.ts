import { Schema, model, Document } from 'mongoose';
import { BaseTime, preSaveAddBaseTime } from './base';

export interface User {
    mobile: string;
}

export interface UserModel extends User, Document {}

const modelSchema = new Schema({
    mobile: { type: String, required: true, index: { unique: true } }
});

////// Create Model /////

export const UserModel = model<UserModel>('User', modelSchema);

////// Functions ////////


export function getUsers(limit = 100) {
    return UserModel.find().limit(limit);
}

export function getUserById(id: string) {
    return UserModel.findOne({ _id: id });
}

export function getUserByUserMobile (mobile) {
    return UserModel.findOne({ mobile });
}

export function addUser(input: User) {
    console.log('input db: ', input)
    const rec = new UserModel({mobile:input});
    rec.save();
    return rec;
}

export function removeUser(id) {
    return UserModel.findByIdAndRemove(id);
}
