import { Schema, model, Document } from 'mongoose';
import { BaseTime, preSaveAddBaseTime } from './base';

export interface Bedrooms {
    guests: number;
    bedrooms: [Bedroom];
}

export interface Bedroom {
    single: number;
    double: number;
    bedrooms: Bedrooms;
}

export interface BedroomModel extends Bedroom, BaseTime, Document {}

export interface BedroomsModel extends Bedrooms, BaseTime, Document {}

const modelSchema = new Schema({
    guests: { type: Number, default: 1},
    bedrooms: [{ type: Schema.Types.ObjectId, ref: 'Bedroom' }]
});

const bedroomModelSchema = new Schema({
    single: { type: Number, default: 0},
    double: { type: Number, default: 0},
    bedrooms: [{ type: Schema.Types.ObjectId, ref: 'Bedrooms' }]
});


modelSchema.pre('save', preSaveAddBaseTime);

bedroomModelSchema.pre('save', preSaveAddBaseTime);


////// Create Model /////

export const BedroomsModel = model<BedroomsModel>('Bedrooms', modelSchema);

export const BedroomModel = model<BedroomModel>('Bedroom', bedroomModelSchema);

////// Functions ////////

export function getBedroomsById(id: string) {
    return BedroomsModel.findById({ _id: id });
}

export function addBedrooms(input: Bedrooms) {
    const rec = BedroomsModel.create(input);
    return rec;
}

export function getBedroomById(id: string) {
    return BedroomModel.findById({ _id: id });
}

export function addBedroom(input: Bedroom) {
    const rec = BedroomModel.create(input);
    return rec;
}

