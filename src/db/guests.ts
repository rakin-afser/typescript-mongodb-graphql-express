import { Schema, model, Document } from 'mongoose';
import { BaseTime, preSaveAddBaseTime } from './base';

export interface Guests {
    guests: string;
    bedroom: string;
}

export interface GuestsModel extends Guests, BaseTime, Document {}

const modelSchema = new Schema({
    guests: { type: String, required: true},
    bedroom: { type: String, required: true }
});

modelSchema.pre('save', preSaveAddBaseTime);


////// Create Model /////

export const GuestsModel = model<GuestsModel>('Guests', modelSchema);

////// Functions ////////

export function getGuestsById(id: string) {
    return GuestsModel.findById({ _id: id });
}

export function addGuests(input: Guests) {
    const rec = GuestsModel.create(input);
    return rec;
}

