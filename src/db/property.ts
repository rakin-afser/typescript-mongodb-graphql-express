import { Schema, model, Document } from 'mongoose';
import { BaseTime, preSaveAddBaseTime } from './base';

export interface Property {
    name: string;
    roomType: string;
    primarilySetUpForGuests: string;
}

export interface PropertyModel extends Property, BaseTime, Document { }

const modelSchema = new Schema({
    name: { type: String, required: true },
    roomType: String,
    primarilySetUpForGuests: String
});

modelSchema.pre('save', preSaveAddBaseTime);

////// Create Model /////

export const PropertyModel = model<PropertyModel>('Property', modelSchema);

////// Functions ////////

export function addProperty(input: Property) {
    const rec = new PropertyModel(input);
    rec.save();
    return rec;
}
export function getPropertyById(id: string) {
    return PropertyModel.findById(id);
}
export function removeProperty(root, { id }) {
    return PropertyModel.findByIdAndRemove(id);
}
