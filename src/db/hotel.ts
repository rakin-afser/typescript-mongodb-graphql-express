import { Schema, model, Document } from 'mongoose';
import { BaseTime, preSaveAddBaseTime } from './base';

export interface Hotel {
    name: string;
    description: string;
    email: string,
    location: string,
    price: string,
    reviewText: string,
    reviewPoint: string,
    img: string
}

export interface HotelModel extends Hotel, BaseTime, Document { }

const modelSchema = new Schema({
    name: { type: String, required: true },
    description: String,
    email: String,
    location: String,
    price: String,
    reviewText: String,
    reviewPoint: String,
    img: String
});

modelSchema.pre('save', preSaveAddBaseTime);

////// Create Model /////

export const HotelModel = model<HotelModel>('Hotel', modelSchema);

////// Functions ////////

export function getHotels(limit = 100) {
    return HotelModel.find().limit(limit);
}

export function addHotel(input: Hotel) {
    const rec = new HotelModel(input);
    rec.save();
    return rec;
}
export function getHotelById(id: string) {
    return HotelModel.findById(id);
}
export function removeHotel(root, { id }) {
    return HotelModel.findByIdAndRemove(id);
}
