require('dotenv').config()

export default {
  db: process.env.DB,
  port: process.env.GRAPHQL_PORT,
  db_name: process.env.MONGODB_DATABASE_NAME,
  session_secret: process.env.SESSION_SECRET,
  another_session_secret: process.env.ANOTHER_SESSION_SECRET,
  fb_app_id: process.env.FB_APP_ID,
  fb_app_state: process.env.FB_APP_STATE,
  fb_app_secret: process.env.FB_APP_SECRET,
}