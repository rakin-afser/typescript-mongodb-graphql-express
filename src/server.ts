import * as mongoose from 'mongoose';
import * as express from 'express';
import * as graphqlHTTP from 'express-graphql';
import * as morgan from 'morgan';
import * as cors from 'cors';
import * as errorHandler from 'errorhandler';
import * as bodyParser from 'body-parser';
import { printSchema } from 'graphql/utilities/schemaPrinter';
import config from './config/Config'
import { graphqlSchema } from './graphql_schema';
// import graphqlSchema  from './graphql_schema/schema/TestSchema';
import { setupPassportAuth, onlyAuthorized } from './authenticate';

// Use node like promise for mongoose
(mongoose as any).Promise = global.Promise;

const DEBUG_MODE = true;
const GRAPHQL_PORT = config.port;
const MONGODB_CONNECTION_URI = `${config.db + config.db_name}`;

// Main App
const app = express();
app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header(
      "Access-Control-Allow-Headers",
      "Origin, X-Requested-With, Content-Type, Accept"
    );
    next();
  });
// Setup MongoDb connection
mongoose.connect(MONGODB_CONNECTION_URI, { useMongoClient: true });

// Express morgan logs
app.use(morgan('combined'));

// Parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({
    extended: true
}));

// Parse application/json
app.use(bodyParser.json())
// Example routes
app.get('/', (req, res) => {
    res.status(200).send('Server endpoint');
});

// Set Auth
setupPassportAuth(app, DEBUG_MODE);

app.use('/graphql',
    cors(),
    // onlyAuthorized(),
    graphqlHTTP(request => {
        // onlyAuthorized(request);
        const startTime = Date.now();
        return {
            schema: graphqlSchema,
            graphiql: true,
            extensions({ document, variables, operationName, result }) {
                return { runTime: Date.now() - startTime };
            }
        };
    })
);

app.use('/schema',
    // onlyAuthorized(),
    (req, res, _next) => {
        res.set('Content-Type', 'text/plain');
        res.send(printSchema(graphqlSchema));
    }
);

app.use(errorHandler());

app.listen(GRAPHQL_PORT);

console.log(`Server started on http://localhost:${GRAPHQL_PORT}/`);
