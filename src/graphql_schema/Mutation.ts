import {
    GraphQLObjectType,
} from 'graphql';

// import { UserSchema } from './schema/User';
import { FbUserSchema } from './schema/FbUser';
import { GuestsSchema } from './schema/Guests';
import { BedroomsSchema } from './schema/Bedrooms';
import { PropertySchema } from './schema/Property';
import { HotelSchema } from './schema/Hotel';
import { ProductSchema } from './product';

var MovieSchema = `
  type Movie {
   name: String
  }
  input MovieInput {
   name: String
  }
  mutation {
   addMovies(movies: [MovieInput]): [Movie]
  }
`

export const mutation = new GraphQLObjectType({
    name: 'Mutation',
    fields: () => Object.assign(
        FbUserSchema.mutation,
        HotelSchema.mutation,
        PropertySchema.mutation,
        GuestsSchema.mutation,
        ProductSchema.mutation
    )
});