import {
    GraphQLSchema
} from 'graphql';

// Import each models schema
import { UserSchema } from './schema/User';
// import { BedroomsSchema } from './schema/Bedrooms';
import { HotelSchema } from './schema/Hotel';
import { PropertySchema } from './schema/Property';
import { ProductSchema } from './product';
import { query } from './Query'
import { mutation } from './Mutation'



export const graphqlSchema = new GraphQLSchema({
    query,
    mutation,
    // subscription: new GraphQLObjectType({
    //     name: 'Subscription',
    //     fields: () => Object.assign(
    //         UserSchema.subscription,
    //         ProductSchema.subscription,
    //     )
    // }),
    types: [
        ...ProductSchema.types,
        ...UserSchema.types,
        ...HotelSchema.types,
        ...PropertySchema.types
    ]
});
