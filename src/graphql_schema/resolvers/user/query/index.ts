import {
    GraphQLList,
    GraphQLString,
    GraphQLInt
} from 'graphql';
import { type } from '../types'

import { getUsers } from '../../../../db/fbUser';
export const query = {
    users: {
        type: new GraphQLList(type),
        args: {
            limit: {
                description: 'limit items in the results',
                type: GraphQLInt
            }
        },
        resolve: (root, { limit }) => getUsers(limit)
    },
    userByUsername: {
        type,
        args: {
            username: {
                description: 'find by username',
                type: GraphQLString
            }
        },
        // resolve: (root, { username }) => getUserByUsername(username)
    },
};