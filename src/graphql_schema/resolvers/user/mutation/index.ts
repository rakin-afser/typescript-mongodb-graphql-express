import {
    GraphQLNonNull,
    GraphQLString
} from 'graphql';

import { type } from '../types'
import {  addUser } from '../../../../db/fbUser';

export const mutation = {
    addUser: {
        type,
        args: {
            username: {
                type: new GraphQLNonNull(GraphQLString)
            },
            password: {
                type: new GraphQLNonNull(GraphQLString)
            },
        },
        resolve: (obj, input) => addUser(input)
    },
};