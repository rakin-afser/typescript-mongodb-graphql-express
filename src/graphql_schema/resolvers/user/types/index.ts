import {
    GraphQLObjectType,
    GraphQLString
} from 'graphql';

export const type = new GraphQLObjectType({
    name: 'User',
    description: 'Auth user',
    fields: () => ({
        username: {
            type: GraphQLString,
            description: 'The username',
        },
        password: {
            type: GraphQLString,
            description: 'The password',
        },
    }),
});