import {
    GraphQLNonNull,
    GraphQLString
} from 'graphql';

import { type } from '../types'
import { addProperty } from '../../../../db/property';

export const mutation = {
    addProperty: {
        type,
        args: {
            name: {
                type: new GraphQLNonNull(GraphQLString)
            },
            roomType: {
                type: new GraphQLNonNull(GraphQLString)
            },
            primarilySetUpForGuests: {
                type: new GraphQLNonNull(GraphQLString)
            }
        },
        resolve: (obj, input) => addProperty(input)
    },
};