import {
    GraphQLList,
    GraphQLString,
    GraphQLInt
} from 'graphql';
import { type } from '../types'

import { getPropertyById } from '../../../../db/property';
export const query = {
    getPropertyById: {
        type,
        args: {
            _id: {
                description: 'find by property id',
                type: GraphQLString
            }
        },
        resolve: (root, { _id }) => getPropertyById(_id)
    },
};