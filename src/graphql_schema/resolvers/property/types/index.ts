import {
    GraphQLObjectType,
    GraphQLString,
    GraphQLEnumType
} from 'graphql';

const propertyTypeEnum = new GraphQLEnumType({
    name: 'ProductType',
    description: 'Types of products',
    values: {
        FOOD: {
            value: 1,
            description: 'Food.',
        },
        DRINK: {
            value: 2,
            description: 'Drink',
        },
    }
});

export const type = new GraphQLObjectType({
    name: 'Property',
    description: 'Room property type',
    fields: () => ({
        name: {
            type: GraphQLString,
            description: 'The room property type',
        },
        roomType: {
            type: GraphQLString,
            description: 'The room type',
        },
        primarilySetUpForGuests: {
            type: GraphQLString,
            description: "It's primarily set up for guests",
        }
    }),
});