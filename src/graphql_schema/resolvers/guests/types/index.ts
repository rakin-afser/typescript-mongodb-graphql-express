import {
    GraphQLObjectType,
    GraphQLString,
    GraphQLInt
} from 'graphql';

export const type = new GraphQLObjectType({
    name: 'Guests',
    description: 'Add Number of Guests With Number of Deadroom',
    fields: () => ({
        guests: {
            type: GraphQLInt,
            description: 'The Total Number of Guests',
        },
        bedroom: {
            type: GraphQLString,
            description: 'The number of bedroom guests can use',
        }
    }),
});