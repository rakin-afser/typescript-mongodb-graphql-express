import {
    GraphQLNonNull,
    GraphQLString,
    GraphQLInt
} from 'graphql';

import { type } from '../types'
import { addGuests } from '../../../../db/guests';

export const mutation = {
    addGuestsWithBedRoom: {
                        type,
                        args: {
                            guests: {
                                type: new GraphQLNonNull(GraphQLInt)
                            },
                            bedroom: {
                                type: new GraphQLNonNull(GraphQLString)
                            }
                        },
                        resolve: (obj, input) => {
                            console.log(".....",input);
                            // addGuests(input)
                        }
    },
};