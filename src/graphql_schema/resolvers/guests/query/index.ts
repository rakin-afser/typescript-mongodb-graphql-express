import {
    GraphQLString
} from 'graphql';
import { type } from '../types'

import { getGuestsById } from '../../../../db/guests';
export const query = {
    guestsById: {
        type,
        args: {
            id: {
                description: 'find by number of guests by id',
                type: GraphQLString
            }
        },
        resolve: (root, { id }) => getGuestsById(id)
    },
};