import {
    GraphQLObjectType,
    GraphQLString,
    GraphQLList,
    GraphQLInt
} from 'graphql';

export const BedroomType = new GraphQLObjectType({
    name: 'Bedroom',
    description: 'Add Number of single and double Ded',
    fields: () => ({
        single: {
            type: GraphQLInt,
            description: 'The number of single bed',
        },
        double: {
            type: GraphQLInt,
            description: 'The number of double bed',
        }
    }),
});


export const BedroomsType = new GraphQLObjectType({
    name: 'Bedrooms',
    description: 'Add Number of Guests With Number of Deadroom',
    fields: () => ({
        guests: {
            type: GraphQLInt,
            description: 'The Total Number of Guests',
        },
        bedrooms: {
            type: new GraphQLList(BedroomType),
            description: 'The number of bedroom guests can use',
        }
    }),
});


export const Bedrooms = `
  type Model { 
    _id: String,
    guests: Number!, 
    bedrooms: [Bedroom]
  }`