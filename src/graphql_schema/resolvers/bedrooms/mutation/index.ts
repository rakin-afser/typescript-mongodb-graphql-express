import {GraphQLObjectType,
    GraphQLNonNull,
    GraphQLList,
    GraphQLString,
    GraphQLInt
} from 'graphql';

import { Bedrooms , BedroomType } from '../types'
import { addBedrooms } from '../../../../db/bedrooms';


export const mutation = {
    addBedRooms: {
        type: Bedrooms,
        args: {
            guests: {
                type: new GraphQLNonNull(GraphQLInt)
            },
            bedrooms: {
                type: new GraphQLList(BedroomType)
            }
        },
        resolve: (obj, input) => {
            console.log(".....",input);
            // addGuests(input)
        }
    },

};