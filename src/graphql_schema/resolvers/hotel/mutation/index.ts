import {
    GraphQLNonNull,
    GraphQLString
} from 'graphql';

import { type } from '../types'
import {  addHotel } from '../../../../db/hotel';

export const mutation = {
    addHotel: {
        type,
        args: {
            name: {
                type: new GraphQLNonNull(GraphQLString)
            },
            description: {
                type: new GraphQLNonNull(GraphQLString)
            },
            email: {
                type: new GraphQLNonNull(GraphQLString)
            },
            location: {
                type: new GraphQLNonNull(GraphQLString)
            },
            price: {
                type: new GraphQLNonNull(GraphQLString)
            },
            reviewText: {
                type: new GraphQLNonNull(GraphQLString)
            },
            reviewPoint: {
                type: new GraphQLNonNull(GraphQLString)
            },
            img: {
                type: new GraphQLNonNull(GraphQLString)
            },
        },
        resolve: (obj, input) => addHotel(input)
    },
};