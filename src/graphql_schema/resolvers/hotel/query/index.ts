import {
    GraphQLList,
    GraphQLString,
    GraphQLInt
} from 'graphql';
import { type } from '../types'

import { getHotels, getHotelById } from '../../../../db/hotel';
export const query = {
    hotels: {
        type: new GraphQLList(type),
        args: {
            limit: {
                description: 'limit items in the results',
                type: GraphQLInt
            }
        },
        resolve: (root, { limit }) => getHotels(limit)
    },
    hotelById: {
        type,
        args: {
            id: {
                description: 'find by hotel id',
                type: GraphQLString
            }
        },
        resolve: (root, { id }) => getHotelById(id)
    },
};