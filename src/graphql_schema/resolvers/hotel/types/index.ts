import {
    GraphQLObjectType,
    GraphQLString
} from 'graphql';

export const type = new GraphQLObjectType({
    name: 'Hotel',
    description: 'Auth Hotel',
    fields: () => ({
        id: {
            type: GraphQLString,
            description: " The hotel id"
        },
        name: {
            type: GraphQLString,
            description: 'The hotel name',
        },
        description: {
            type: GraphQLString,
            description: 'Tell us something about your hotel',
        },
        email: {
            type: GraphQLString,
            description: 'Input your contact email'
        },
        location: {
            type: GraphQLString,
            description: 'Insert your hotel location'
        },
        price: {
            type: GraphQLString,
            description: 'Add your room price'
        },
        reviewText: {
            type: GraphQLString,
            description: 'customer review description'
        },
        reviewPoint: {
            type: GraphQLString,
            description: 'customer review point'
        },
        img: {
            type: GraphQLString,
            description: 'hotel image'
        },
    }),
});