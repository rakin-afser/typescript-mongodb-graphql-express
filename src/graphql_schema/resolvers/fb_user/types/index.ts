import {
    GraphQLObjectType,
    GraphQLString
} from 'graphql';

export const type = new GraphQLObjectType({
    name: 'FbUser',
    description: 'FB Auth user',
    fields: () => ({
        token: {
            type: GraphQLString,
            description: 'The fb token',
        },
        mobile: {
            type: GraphQLString,
            description: 'The facbook verify mobile no',
        }
    }),
});