import {
    GraphQLList,
    GraphQLString,
    GraphQLInt
} from 'graphql';
import { type } from '../types'

import { getUserByUserMobile } from '../../../../db/fbUser';
export const query = {
    fbUserByUserToken: {
        type,
        args: {
            mobile: {
                description: 'find by facbook verify mobile no',
                type: GraphQLString
            }
        },
        resolve: (root, { mobile }) => getUserByUserMobile(mobile)
    },
};