import {
    GraphQLNonNull,
    GraphQLString
} from 'graphql';

import { type } from '../types'
import { addUser } from '../../../../db/fbUser';

export const mutation = {
    addFBVerifyUser: {
                        type,
                        args: {
                            code: {
                                type: new GraphQLNonNull(GraphQLString)
                            },
                            mobile: {
                                type: new GraphQLNonNull(GraphQLString)
                            }
                        },
                        resolve: (obj, input) => {
                            console.log(".....",input);
                            // addFBVerifyUser(input)
                        }
    },
};