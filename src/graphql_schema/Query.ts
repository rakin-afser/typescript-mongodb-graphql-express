import {
    GraphQLObjectType,
} from 'graphql';

// import { UserSchema } from './schema/User';
import { FbUserSchema } from './schema/FbUser';
import { GuestsSchema } from './schema/Guests';
import { HotelSchema } from './schema/Hotel';
import { ProductSchema } from './product';

export const query = new GraphQLObjectType({
    name: 'Query',
    fields: () => Object.assign(
        FbUserSchema.query,
        HotelSchema.query,
        GuestsSchema.query,
        ProductSchema.query
    )
});