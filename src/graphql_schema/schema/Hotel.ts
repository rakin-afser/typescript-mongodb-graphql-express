import { type } from '../resolvers/hotel/types'
import { query } from '../resolvers/hotel/query'
import { mutation } from '../resolvers/hotel/mutation'


export const HotelSchema = {
    query,
    mutation,
    types: [type]
};