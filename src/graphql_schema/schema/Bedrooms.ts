import { Bedrooms } from '../resolvers/bedrooms/types'
import { query } from '../resolvers/bedrooms/query'
import { mutation } from '../resolvers/bedrooms/mutation'

const subscription = {

};

export const BedroomsSchema = {
    query,
    mutation,
    subscription,
    types: [Bedrooms]
};