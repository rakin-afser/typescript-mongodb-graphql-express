import { type } from '../resolvers/property/types'
import { query } from '../resolvers/property/query'
import { mutation } from '../resolvers/property/mutation'


export const PropertySchema = {
    query,
    mutation,
    types: [type]
};