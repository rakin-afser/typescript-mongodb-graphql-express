import { type } from '../resolvers/user/types'
import { query } from '../resolvers/user/query'
import { mutation } from '../resolvers/user/mutation'

const subscription = {

};

export const UserSchema = {
    query,
    mutation,
    subscription,
    types: [type]
};