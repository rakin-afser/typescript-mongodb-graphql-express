import { type } from '../resolvers/guests/types'
import { query } from '../resolvers/guests/query'
import { mutation } from '../resolvers/guests/mutation'

const subscription = {

};

export const GuestsSchema = {
    query,
    mutation,
    subscription,
    types: [type]
};