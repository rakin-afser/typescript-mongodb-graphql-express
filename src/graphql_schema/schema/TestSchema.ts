var { buildSchema } = require('graphql');

// Construct a schema, using GraphQL schema language
export default buildSchema(`
  input MessageInput {
    content: String
    author: String
  }

  type Message {
    id: ID!
    content: String
    author: String
  }

  type Movie {
    name: String
   }

   input MovieInput {
    name: String
   }89797/+


  type Query {
    getMessage(id: ID!): Message
  }

  type Mutation {
    addMovies(movies: [MovieInput]): [Movie]
    createMessage(input: MessageInput): Message
    updateMessage(id: ID!, input: MessageInput): Message
  }
`);

