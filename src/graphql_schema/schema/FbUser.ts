import { type } from '../resolvers/fb_user/types'
import { query } from '../resolvers/fb_user/query'
import { mutation } from '../resolvers/fb_user/mutation'

const subscription = {

};

export const FbUserSchema = {
    query,
    mutation,
    subscription,
    types: [type]
};