import * as passport from 'passport';
import fetch from 'node-fetch';
import * as jwt from 'jsonwebtoken';
import { Application, Request, Response, RequestHandler } from 'express';
import { Strategy, StrategyOptions, ExtractJwt, VerifiedCallback } from 'passport-jwt';
import config from './config/Config'

import {addUser, getUserByUserMobile } from './db/fbUser'


const APP_ID = "285999858818729";
const APP_STATE = "4adfaafb-c934-7e8c-8aa3-12c5918a4b55";
const APP_SECRET = "cd931dd4382514f1697fb659a5247a2c";

let localStorage;
if (typeof localStorage === "undefined" || localStorage === null) {
    var LocalStorage = require('node-localstorage').LocalStorage;
    localStorage = new LocalStorage('./scratch');
  }

  interface JwtPayload {
    mobileNumber: string;
    date: number;
  }

  const JWT_SECRET_KEY = 'PRIVATE_AUTH_KEY';

const jwtOptions: StrategyOptions = {
    jwtFromRequest: ExtractJwt.fromAuthHeader(),
    secretOrKey: JWT_SECRET_KEY,
    passReqToCallback: true,
};

const jwtStategy = new Strategy(
    jwtOptions,
    (req: Request, jwtPayload: JwtPayload, done: VerifiedCallback) => {

        if(!jwtPayload.mobileNumber){
            console.log('No mobile number in jwtPayload');
        }
        // query in db
        getUserByUserMobile(jwtPayload.mobileNumber)
        .then((user) => {
            if (user) {
                return done(null, user);
            } else {
                return done(null, false);
                // TODO: handle custom error to ask for create a new account
            }
        }).catch(err => {
            return done(err, false);
        });

    }
)


/**
 * If added to a express route, it will make the route require the auth token
 */
export function onlyAuthorized() {
    console.log('call only authorized')
    return passport.authenticate('jwt', { session: false });
}

// export function onlyAuthorized(req) {
//         const reqToken =  req.headers.authorization
//         const resToken =  localStorage.getItem(reqToken)
//         console.log("token verify", reqToken === resToken);
// }

/**
 * Setup the Passport JWT for the given express App.
 * It will add the auth routes (auth, login, logout) to handle the token authorization
 * It will use the mongoDB UserModel to check for user and password
 * Set addDebugRoutes to true for adding a Auth Form for testing purpose
 */
export function setupPassportAuth(app: Application, addDebugRoutes = false) {
    passport.use(jwtStategy);

    app.use(passport.initialize());

    app.post('/login', async (req, res, next) => {
        try {
            const { code, mobile } = req.body;
    
            if (!code && !mobile) {
                // is not work
                const inputError = {"error":"code or mobile not set in the request"};
                return res.json(inputError);
                console.log('code or mobile not set in the request');
            }
    
            // Get user by mobile
            let user: any = await getUserByUserMobile(mobile);
    
            

            // If user not exist, store user into database;
            if (!user) {
                // console.log('user not exists');
                // Store user into DB;
                addUser(mobile);
                user = { mobile }
            }
    
            let url = 'https://graph.accountkit.com/v1.3/access_token?grant_type=authorization_code&code=' + code + '&access_token=AA|' + config.fb_app_id + '|' + config.fb_app_secret;
    
            let newToken; 
            await fetch(url).
            then(response => response.json()).then((repos) => {
                // console.log('fetch response', )
                newToken = repos;
            });

            const jwtPayload: JwtPayload = {
                // user_token: `${user.mobile.toString()}.${Date.now()}`,
                mobileNumber: user.mobile.toString(),
                date: Date.now()
            };
            let token;
            if(newToken.access_token){
                token = jwt.sign(jwtPayload, jwtOptions.secretOrKey);
            }
            console.log('server token', user.mobile.toString())
            // const jwtPayload = newToken.access_token;
            // // Return a sign token containing the user ID (JwtPayload)
            // const token = jwt.sign(jwtPayload, 'PRIVATE_AUTH_KEY');
            // localStorage.setItem(token, `${mobile}.${Date.now()}`)
            res.json({ token } );
    
        } catch (error) {
            res.json({
                error: error.message
            });
        }
    });

    app.get('/logout', (req, res) => {
        // 
        req.logout();
        res.redirect('/');
    });
}
